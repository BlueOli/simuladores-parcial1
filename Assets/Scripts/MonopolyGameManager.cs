﻿using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using TMPro;

namespace Assets.Scripts.Classes
{
    public class MonopolyGameManager : MonoBehaviour
    {
        Nivel nivel;
        GameObject casillero, casilleroEsquina, casilleroPropiedad, casilleroFerrocarril, casilleroServicio, terrenoNombre, terrenoValor, fichaJugador;
        List<Jugador> jugadores = new List<Jugador>();
        Stack<Jugador> turnosJugadores = new Stack<Jugador>();
        
        Jugador subastador;
        int valorSubasta = 10;

        Jugador comprador;

        #region Indices

        int indicePropiedad = 0;
        int indiceServicio = 0;
        int indiceFerrocarril = 0;

        #endregion

        string tipoTerreno = "";
        string tipoTerrenoCasillero = "";

        #region Botones
        public Button botonDeclararBancarrota;

        public Button botonTirarDados;
        public Button botonMostrarTarjeta;
        public Button botonOcultarTarjeta;
        public Button botonTerminarTurno;
        public Button botonComprarTerreno;
        public Button botonIniciarSubasta;
        public Button botonPagarRenta;
        public Button botonValorSubasta;
        public Button botonSubasta10Arriba;
        public Button botonSubasta10Abajo;
        public Button botonSalirSubasta;
        public Button botonVenderTerreno;
        public Button botonHipotecarTerreno;
        public Button botonPagarHipoteca;

        public Button botonSiguienteTerreno;
        public Button botonAnteriorTerreno;
        public Button botonPropiedad;
        public Button botonServicio;
        public Button botonFerrocarril;

        public Button botonComprarCasa;
        public Button botonVenderCasa;
        public Button botonComprarHotel;
        public Button botonVenderHotel;

        public Button botonArcaComunal;
        public Button botonFortuna;

        public GameObject victoria;
        #endregion

        public GameObject CartaPropiedad;
        public GameObject CartaServicio;
        public GameObject CartaFerrocarril;

        public List<TextMeshProUGUI> puntajes;

        public TMP_Dropdown dpdwnJugadores;

        bool movimientoJugador = true;

        string nombreNivelActual = "Nivel 1";

        private void Start()
        {
            #region Objetos Instanciados: Inicializacion
            
            casillero = MonopolyLevelManager.instancia.damelstPrefabsMonopoly().Find(x => x.name == "Casillero");
            casilleroEsquina = MonopolyLevelManager.instancia.damelstPrefabsMonopoly().Find(x => x.name == "CasilleroEsquina");
            casilleroPropiedad = MonopolyLevelManager.instancia.damelstPrefabsMonopoly().Find(x => x.name == "CasilleroPropiedad");
            casilleroFerrocarril = MonopolyLevelManager.instancia.damelstPrefabsMonopoly().Find(x => x.name == "CasilleroFerrocarril");
            casilleroServicio = MonopolyLevelManager.instancia.damelstPrefabsMonopoly().Find(x => x.name == "CasilleroServicio");
            terrenoNombre = MonopolyLevelManager.instancia.damelstPrefabsMonopoly().Find(x => x.name == "TerrenoNombre");
            terrenoValor = MonopolyLevelManager.instancia.damelstPrefabsMonopoly().Find(x => x.name == "TerrenoValor");
            fichaJugador = MonopolyLevelManager.instancia.damelstPrefabsMonopoly().Find(x => x.name == "FichaJugador");

            #endregion

            #region Botones: Inicializacion
            botonDeclararBancarrota.interactable = false;

            botonTerminarTurno.interactable = false;
            botonTirarDados.interactable = true;
            botonMostrarTarjeta.interactable = false;
            botonOcultarTarjeta.interactable = false;
            botonComprarTerreno.interactable = false;
            botonIniciarSubasta.interactable = false;
            botonPagarRenta.interactable = false;
            botonValorSubasta.interactable = false;
            botonSubasta10Abajo.interactable = false;
            botonSubasta10Arriba.interactable = false;
            botonSalirSubasta.interactable = false;

            botonAnteriorTerreno.interactable = false;
            botonSiguienteTerreno.interactable = false;
            botonPropiedad.interactable = false;
            botonServicio.interactable = false;
            botonFerrocarril.interactable = false;

            botonVenderTerreno.interactable = false;
            botonHipotecarTerreno.interactable = false;
            botonPagarHipoteca.interactable = false;
            dpdwnJugadores.interactable = false;

            botonComprarCasa.interactable = false;
            botonVenderCasa.interactable = false;
            botonComprarHotel.interactable = false;
            botonVenderHotel.interactable = false;

            botonArcaComunal.interactable = false;
            botonFortuna.interactable = false;

            #endregion

            #region Tarjetas: Inicializacion
            CartaPropiedad.SetActive(false);
            CartaFerrocarril.SetActive(false);
            CartaServicio.SetActive(false);
            #endregion

            AgregarJugadores(jugadores);

            CargarNivel(nombreNivelActual);

            DefinirTurnos(jugadores, turnosJugadores);

            MostrarJugador(turnosJugadores.Peek());

            CargarDropdownJugadores();

            ConsultaTerrenosAdquiridos();

            botonDeclararBancarrota.interactable = true;

            int indice = 0;
            foreach (TextMeshProUGUI txtMesh in puntajes)
            {
                txtMesh.color = InstanciadorPrefabs.instancia.damelstMaterialesMonopoly().Find(x => x.name == jugadores[indice].Color).color;
                txtMesh.text = jugadores[indice].Color + " - $" + jugadores[indice].Dinero;
                if (jugadores[indice] == turnosJugadores.Peek()) txtMesh.fontStyle = FontStyles.Underline;
                else txtMesh.fontStyle = FontStyles.Normal;
                indice++;
            }
        }


        #region Victoria

        private bool ChequearVictoria()
        {
            if (turnosJugadores.Count == 1)
            {
                victoria.SetActive(true);
                victoria.transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text =
                    ("¡Has ganado " + turnosJugadores.Peek().Color + "!");
                return true;
            }
            else return false;
        }

        public void CerrarJuego()
        {
            #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            #else
                Application.Quit();
            #endif
        }

        #endregion

        #region Quiebra

        public void DeclararQuiebraAlBanco()
        {
            Jugador mJugador = turnosJugadores.Peek();

            mJugador.DeclararQuiebra();

            EliminarJugador();
        }

        private void EliminarJugador()
        {
            Jugador mJugador = turnosJugadores.Peek();

            jugadores.Remove(mJugador);
            InstanciadorPrefabs.instancia.EliminarFicha(mJugador);
            Debug.Log("El jugador " + turnosJugadores.Pop().Color + " ha sido eliminado.");

            if (!ChequearVictoria())
            {
                MostrarJugador(turnosJugadores.Peek());
                botonTirarDados.interactable = true;
                botonTerminarTurno.interactable = false;
                movimientoJugador = true;
                TextMeshProUGUI txtValor = botonValorSubasta.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
                valorSubasta = Int32.Parse(txtValor.text);

                OcultarTarjeta();

                EvaluarTerreno();

                ConsultaTerrenosAdquiridos();
            }
        }

        #endregion

        #region Casas y Hoteles

        #region Comprar y Vender

        public void ComprarCasa()
        {
            Jugador tmpJugador = turnosJugadores.Peek();
            Propiedad tmpPropiedad = tmpJugador.Propiedades[indicePropiedad];

            tmpJugador.QuitarDinero(tmpPropiedad.CostoPorCasa);
            tmpPropiedad.Casas++;

            OcultarTarjeta();
            MostrarPropiedad();
        }

        public void VenderCasa()
        {
            Jugador tmpJugador = turnosJugadores.Peek();
            Propiedad tmpPropiedad = tmpJugador.Propiedades[indicePropiedad];

            tmpJugador.AdquirirDinero(tmpPropiedad.CostoPorCasa / 2);
            tmpPropiedad.Casas--;

            if (tmpPropiedad.Casas == 0)
            {
                botonHipotecarTerreno.interactable = true;
            }
            else
            {
                botonHipotecarTerreno.interactable = false;
            }

            OcultarTarjeta();
            MostrarPropiedad();
        }

        public void ComprarHotel()
        {
            Jugador tmpJugador = turnosJugadores.Peek();
            Propiedad tmpPropiedad = tmpJugador.Propiedades[indicePropiedad];

            tmpJugador.QuitarDinero(tmpPropiedad.CostoPorHotel);
            tmpPropiedad.Hoteles++;

            OcultarTarjeta();
            MostrarPropiedad();
        }

        public void VenderHotel()
        {
            Jugador tmpJugador = turnosJugadores.Peek();
            Propiedad tmpPropiedad = tmpJugador.Propiedades[indicePropiedad];

            tmpJugador.AdquirirDinero(tmpPropiedad.CostoPorHotel);
            tmpPropiedad.Hoteles--;

            if (tmpPropiedad.Casas == 0 && tmpPropiedad.Hoteles == 0)
            {
                botonHipotecarTerreno.interactable = true;
            }
            else
            {
                botonHipotecarTerreno.interactable = false;
            }

            OcultarTarjeta();
            MostrarPropiedad();
        }

        #endregion

        #region Consultar
        public bool ConsultarPropietarioBarrio(Jugador propietario, Propiedad propiedad)
        {
            Barrio tmpBarrio = nivel.Tablero.dameBarrio(propiedad.Color);
            bool propietarioBarrio = true;

            foreach(Propiedad p in tmpBarrio.LstPropiedades)
            {
                if(propietario != p.Propietario)
                {
                    propietarioBarrio = false;
                }
            }

            return propietarioBarrio;
        }

        public bool ConsultarCompraCasa(Jugador propietario, Propiedad propiedad)
        {
            bool puedeComprarCasa = false;
            int cantidadCasasMin = 5;
            int cantidadCasasMax = 0;

            if(ConsultarPropietarioBarrio(propietario, propiedad))
            {
                Barrio tmpBarrio = nivel.Tablero.dameBarrio(propiedad.Color);

                foreach (Propiedad p in tmpBarrio.LstPropiedades)
                {
                    if(p.Casas > cantidadCasasMax)
                    {
                        cantidadCasasMax = p.Casas;
                    }
                    if(p.Casas < cantidadCasasMin)
                    {
                        cantidadCasasMin = p.Casas;
                    }
                }

                if ((cantidadCasasMax - (propiedad.Casas + 1)) < 2 &&
                ((propiedad.Casas + 1) - cantidadCasasMin) < 2 &&
                (cantidadCasasMax - cantidadCasasMin) < 2)
                {
                    puedeComprarCasa = true;
                }
            }

            if (propiedad.Casas > 3)
            {
                puedeComprarCasa = false;
            }

            return puedeComprarCasa;
        }

        public bool ConsultarVentaCasa(Jugador propietario, Propiedad propiedad)
        {
            bool puedeVenderCasa = false;
            int cantidadCasasMin = 5;
            int cantidadCasasMax = 0;

            if (ConsultarPropietarioBarrio(propietario, propiedad))
            {
                Barrio tmpBarrio = nivel.Tablero.dameBarrio(propiedad.Color);

                foreach (Propiedad p in tmpBarrio.LstPropiedades)
                {
                    if (p.Casas > cantidadCasasMax)
                    {
                        cantidadCasasMax = p.Casas;
                    }
                    if (p.Casas < cantidadCasasMin)
                    {
                        cantidadCasasMin = p.Casas;
                    }
                }

                if ((cantidadCasasMax - (propiedad.Casas - 1)) < 2 &&
                ((propiedad.Casas - 1) - cantidadCasasMin) < 2 &&
                (cantidadCasasMax - cantidadCasasMin) < 2)
                {
                    puedeVenderCasa = true;
                }
            }

            if(propiedad.Casas < 1 || propiedad.Hoteles > 0)
            {
                puedeVenderCasa = false;
            }

            return puedeVenderCasa;
        }

        public bool ConsultarCompraHotel(Jugador propietario, Propiedad propiedad)
        {
            bool puedeComprarHotel = false;

            if (ConsultarPropietarioBarrio(propietario, propiedad))
            {
                puedeComprarHotel = true;

                Barrio tmpBarrio = nivel.Tablero.dameBarrio(propiedad.Color);

                foreach (Propiedad p in tmpBarrio.LstPropiedades)
                {
                    if(p.Casas < 4)
                    {
                        puedeComprarHotel = false;
                    }
                }
            }

            if (propiedad.Hoteles > 0)
            {
                puedeComprarHotel = false;
            }

            return puedeComprarHotel;
        }

        public bool ConsultarVentaHotel(Jugador propietario, Propiedad propiedad)
        {
            bool puedeVenderHotel = false;

            if (ConsultarPropietarioBarrio(propietario, propiedad))
            {
                puedeVenderHotel = true;

                Barrio tmpBarrio = nivel.Tablero.dameBarrio(propiedad.Color);

                foreach (Propiedad p in tmpBarrio.LstPropiedades)
                {
                    if (p.Casas < 4)
                    {
                        puedeVenderHotel = false;
                    }
                }
            }

            if (propiedad.Hoteles < 1)
            {
                puedeVenderHotel = false;
            }

            return puedeVenderHotel;
        }
        #endregion

        #endregion

        #region Venta Tereno

        private void CargarDropdownJugadores()
        {
            List<string> lstColoresJugadores = new List<string>();

            foreach (Jugador j in jugadores)
            {
                lstColoresJugadores.Add(j.Color);
            }

            dpdwnJugadores.AddOptions(lstColoresJugadores);
        }

        public void CambiarComprador()
        {
            if (dpdwnJugadores.value == 0)
            {
                System.Random rnd = new System.Random();
                dpdwnJugadores.value = rnd.Next(1, jugadores.Count() + 1);
            }
            else
            {
                comprador = jugadores[dpdwnJugadores.value - 1];
            }

            switch (tipoTerreno)
            {
                case "Propiedad":
                    if (comprador != turnosJugadores.Peek() &&
                        comprador.Dinero > turnosJugadores.Peek().Propiedades[indicePropiedad].Valor)
                    {
                        botonVenderTerreno.interactable = true;
                    }
                    else
                    {
                        botonVenderTerreno.interactable = false;
                    }
                    break;
                case "Servicio":
                    if (comprador != turnosJugadores.Peek() &&
                        comprador.Dinero > turnosJugadores.Peek().Servicios[indiceServicio].Valor)
                    {
                        botonVenderTerreno.interactable = true;
                    }
                    else
                    {
                        botonVenderTerreno.interactable = false;
                    }
                    break;
                case "Ferrocarril":
                    if (comprador != turnosJugadores.Peek() &&
                         comprador.Dinero > turnosJugadores.Peek().Ferrocarriles[indiceFerrocarril].Valor)
                    {
                        botonVenderTerreno.interactable = true;
                    }
                    else
                    {
                        botonVenderTerreno.interactable = false;
                    }
                    break;
                default:
                    break;
            }
        }

        public void VenderTerreno()
        {
            botonVenderTerreno.interactable = false;

            switch (tipoTerreno)
            {
                case "Propiedad":
                    turnosJugadores.Peek().VenderPropiedad(comprador, indicePropiedad);
                    break;
                case "Servicio":
                    turnosJugadores.Peek().VenderServicio(comprador, indiceServicio);
                    break;
                case "Ferrocarril":
                    turnosJugadores.Peek().VenderFerrocarril(comprador, indiceFerrocarril);
                    break;
                default:
                    break;
            }

            OcultarTarjeta();

            ConsultaTerrenosAdquiridos();

            dpdwnJugadores.SetValueWithoutNotify(0);
        }

        #endregion

        #region Hipoteca

        public void HipotecarTerreno()
        {
            botonHipotecarTerreno.interactable = false;
            botonPagarHipoteca.interactable = true;

            switch (tipoTerreno)
            {
                case "Propiedad":
                    turnosJugadores.Peek().Propiedades[indicePropiedad].Hipotecado = true;
                    turnosJugadores.Peek().AdquirirDinero(turnosJugadores.Peek().Propiedades[indicePropiedad].CalcularHipoteca());
                    break;
                case "Servicio":
                    turnosJugadores.Peek().Servicios[indiceServicio].Hipotecado = true;
                    turnosJugadores.Peek().AdquirirDinero(turnosJugadores.Peek().Servicios[indiceServicio].CalcularHipoteca());
                    break;
                case "Ferrocarril":
                    turnosJugadores.Peek().Ferrocarriles[indiceFerrocarril].Hipotecado = true;
                    turnosJugadores.Peek().AdquirirDinero(turnosJugadores.Peek().Ferrocarriles[indiceFerrocarril].CalcularHipoteca());
                    break;
                default:
                    break;
            }
        }

        public void PagarHipoteca()
        {
            botonPagarHipoteca.interactable = false;
            if (turnosJugadores.Peek().Propiedades[indicePropiedad].Casas == 0 &&
                turnosJugadores.Peek().Propiedades[indicePropiedad].Hoteles == 0)
            {
                botonHipotecarTerreno.interactable = true;
            }
            else
            {
                botonHipotecarTerreno.interactable = false;
            }

            switch (tipoTerreno)
            {
                case "Propiedad":
                    turnosJugadores.Peek().AdquirirDinero(turnosJugadores.Peek().Propiedades[indicePropiedad].CalcularHipoteca());
                    turnosJugadores.Peek().Propiedades[indicePropiedad].Hipotecado = false;
                    break;
                case "Servicio":
                    turnosJugadores.Peek().AdquirirDinero(turnosJugadores.Peek().Servicios[indiceServicio].CalcularHipoteca());
                    turnosJugadores.Peek().Servicios[indiceServicio].Hipotecado = false;
                    break;
                case "Ferrocarril":
                    turnosJugadores.Peek().AdquirirDinero(turnosJugadores.Peek().Ferrocarriles[indiceFerrocarril].CalcularHipoteca());
                    turnosJugadores.Peek().Ferrocarriles[indiceFerrocarril].Hipotecado = false;
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region CartaTerreno

        public void ConsultaTerrenosAdquiridos()
        {
            if (turnosJugadores.Peek().Propiedades.Count != 0)
            {
                botonPropiedad.interactable = true;
                indicePropiedad = 0;
            }
            else
            {
                CartaPropiedad.SetActive(false);
                botonPropiedad.interactable = false;
            }

            if (turnosJugadores.Peek().Servicios.Count != 0)
            {
                botonServicio.interactable = true;
                indiceServicio = 0;
            }
            else
            {
                CartaServicio.SetActive(false);
                botonServicio.interactable = false;
            }

            if (turnosJugadores.Peek().Ferrocarriles.Count != 0)
            {
                botonFerrocarril.interactable = true;
                indiceFerrocarril = 0;
            }
            else
            {
                CartaFerrocarril.SetActive(false);
                botonFerrocarril.interactable = false;
            }
        }

        public void SiguienteTerreno()
        {
            switch (tipoTerreno)
            {
                case "Propiedad":
                    indicePropiedad++;
                    MostrarPropiedad();
                    break;
                case "Servicio":
                    indiceServicio++;
                    MostrarServicio();
                    break;
                case "Ferrocarril":
                    indiceFerrocarril++;
                    MostrarFerrocarril();
                    break;
                default:
                    break;
            }
        }

        public void AnteriorTerreno()
        {
            switch (tipoTerreno)
            {
                case "Propiedad":
                    indicePropiedad--;
                    MostrarPropiedad();
                    break;
                case "Servicio":
                    indiceServicio--;
                    MostrarServicio();
                    break;
                case "Ferrocarril":
                    indiceFerrocarril--;
                    MostrarFerrocarril();
                    break;
                default:
                    break;
            }
        }

        public void MostrarPropiedad()
        {
            Jugador tmpJugador = turnosJugadores.Peek();
            Propiedad tmpPropiedad = tmpJugador.Propiedades[indicePropiedad];

            botonOcultarTarjeta.interactable = true;

            tipoTerreno = "Propiedad";
            MostrarCartaPropiedad(tmpPropiedad);

            if(indicePropiedad+1 < tmpJugador.Propiedades.Count)
            {
                botonSiguienteTerreno.interactable = true;
            }
            else
            {
                botonSiguienteTerreno.interactable = false;
            }

            if (indicePropiedad - 1 > -1)
            {
                botonAnteriorTerreno.interactable = true;
            }
            else
            {
                botonAnteriorTerreno.interactable = false;
            }

            if (tmpPropiedad.Hipotecado)
            {
                botonHipotecarTerreno.interactable = false;
                botonPagarHipoteca.interactable = true;
            }
            else
            {
                botonPagarHipoteca.interactable = false;
                if (tmpPropiedad.Casas == 0 && tmpPropiedad.Hoteles == 0)
                {
                    botonHipotecarTerreno.interactable = true;
                }
                else
                {
                    botonHipotecarTerreno.interactable = false;
                }
            }

            if (ConsultarCompraCasa(tmpJugador, tmpPropiedad))
            {
                botonComprarCasa.interactable = true;
            }
            else
            {
                botonComprarCasa.interactable = false;
            }

            if (ConsultarVentaCasa(tmpJugador, tmpPropiedad))
            {
                botonVenderCasa.interactable = true;
            }
            else
            {
                botonVenderCasa.interactable = false;
            }

            if (ConsultarCompraHotel(tmpJugador, tmpPropiedad))
            {
                botonComprarHotel.interactable = true;
            }
            else
            {
                botonComprarHotel.interactable = false;
            }

            if (ConsultarVentaHotel(tmpJugador, tmpPropiedad))
            {
                botonVenderHotel.interactable = true;
            }
            else
            {
                botonVenderHotel.interactable = false;
            }
        }

        public void MostrarServicio()
        {
            botonOcultarTarjeta.interactable = true;

            tipoTerreno = "Servicio";
            MostrarCartaServicio(turnosJugadores.Peek().Servicios[indiceServicio]);
            if (indiceServicio + 1 < turnosJugadores.Peek().Servicios.Count)
            {
                botonSiguienteTerreno.interactable = true;
            }
            else
            {
                botonSiguienteTerreno.interactable = false;
            }

            if (indiceServicio - 1 > -1)
            {
                botonAnteriorTerreno.interactable = true;
            }
            else
            {
                botonAnteriorTerreno.interactable = false;
            }
        }

        public void MostrarFerrocarril()
        {
            botonOcultarTarjeta.interactable = true;

            tipoTerreno = "Ferrocarril";
            MostrarCartaFerrocarril(turnosJugadores.Peek().Ferrocarriles[indiceFerrocarril]);
            if (indiceFerrocarril + 1 < turnosJugadores.Peek().Ferrocarriles.Count)
            {
                botonSiguienteTerreno.interactable = true;
            }
            else
            {
                botonSiguienteTerreno.interactable = false;
            }

            if (indiceFerrocarril - 1 > -1)
            {
                botonAnteriorTerreno.interactable = true;
            }
            else
            {
                botonAnteriorTerreno.interactable = false;
            }
        }

        public void MostrarTarjeta()
        {
            botonMostrarTarjeta.interactable = false;
            botonOcultarTarjeta.interactable = true;
           
            switch (tipoTerrenoCasillero)
            {
                case "Propiedad":
                    MostrarCartaPropiedad(nivel.Tablero.damePropiedad(turnosJugadores.Peek().Posicion));
                    break;
                case "Servicio":
                    MostrarCartaServicio(nivel.Tablero.dameServicio(turnosJugadores.Peek().Posicion));
                    break;
                case "Ferrocarril":
                    MostrarCartaFerrocarril(nivel.Tablero.dameFerrocarril(turnosJugadores.Peek().Posicion));
                    break;
                default:
                    break;
            }
        }

        public void OcultarTarjeta()
        {
            botonOcultarTarjeta.interactable = false;
            botonSiguienteTerreno.interactable = false;
            botonAnteriorTerreno.interactable = false;

            indicePropiedad = 0;
            indiceServicio = 0;
            indiceFerrocarril = 0;

            CartaPropiedad.SetActive(false);
            CartaFerrocarril.SetActive(false);
            CartaServicio.SetActive(false);

            dpdwnJugadores.interactable = false;
            botonVenderTerreno.interactable = false;
            botonHipotecarTerreno.interactable = false;
            botonPagarHipoteca.interactable = false;

            botonComprarCasa.interactable = false;
            botonVenderCasa.interactable = false;
            botonComprarHotel.interactable = false;
            botonVenderHotel.interactable = false;

            EvaluarTerreno();
        }

        public void MostrarCartaPropiedad(Propiedad propiedad)
        {
            CartaServicio.SetActive(false);
            CartaFerrocarril.SetActive(false);
            CartaPropiedad.SetActive(true);

            Material tmpMaterial = InstanciadorPrefabs.instancia.damelstMaterialesMonopoly().
                                    Find(x => x.name == propiedad.Color);

            if(propiedad.Color == "Amarillo")
            {
                CartaPropiedad.transform.GetChild(1).GetComponent<TextMeshProUGUI>().color = new Color(0,0,0);
            }
            else
            {
                CartaPropiedad.transform.GetChild(1).GetComponent<TextMeshProUGUI>().color = new Color(255, 255, 255);
            }

            CartaPropiedad.transform.GetChild(0).GetComponent<RawImage>().color = tmpMaterial.color;
            CartaPropiedad.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = propiedad.Nombre;
            CartaPropiedad.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "Valor: $" + propiedad.Valor.ToString();
            CartaPropiedad.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = "Alquiler: $" + propiedad.Renta.ToString();
            CartaPropiedad.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = "Con 1 Casa: $" + propiedad.RentaConCasas[0].ToString();
            CartaPropiedad.transform.GetChild(5).GetComponent<TextMeshProUGUI>().text = "Con 2 Casas: $" + propiedad.RentaConCasas[1].ToString();
            CartaPropiedad.transform.GetChild(6).GetComponent<TextMeshProUGUI>().text = "Con 3 Casas: $" + propiedad.RentaConCasas[2].ToString();
            CartaPropiedad.transform.GetChild(7).GetComponent<TextMeshProUGUI>().text = "Con 4 Casas: $" + propiedad.RentaConCasas[3].ToString();
            CartaPropiedad.transform.GetChild(8).GetComponent<TextMeshProUGUI>().text = "Con Hotel: $" + propiedad.RentaConHotel.ToString();
            CartaPropiedad.transform.GetChild(9).GetComponent<TextMeshProUGUI>().text = "Hipoteca: $" + propiedad.Hipoteca.ToString();

            for (int i = 0; i < 4; i++)
            {
                if (i < propiedad.Casas)
                {
                    CartaPropiedad.transform.GetChild(11).GetChild(i).gameObject.SetActive(true);
                }
                else
                {
                    CartaPropiedad.transform.GetChild(11).GetChild(i).gameObject.SetActive(false);
                }
            }

            if(propiedad.Hoteles > 0)
            {
                CartaPropiedad.transform.GetChild(12).transform.gameObject.SetActive(true);
            }
            else
            {
                CartaPropiedad.transform.GetChild(12).transform.gameObject.SetActive(false);
            }

            if (propiedad.Propietario == turnosJugadores.Peek())
            {
                dpdwnJugadores.interactable = true;
            }
        }

        public void MostrarCartaServicio(Servicio servicio)
        {
            CartaPropiedad.SetActive(false);
            CartaFerrocarril.SetActive(false);
            CartaServicio.SetActive(true);

            CartaServicio.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Servicio de " + servicio.Nombre;
            CartaServicio.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Valor: $" + servicio.Valor.ToString();
            CartaServicio.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "Hipoteca: $" + servicio.Hipoteca.ToString();

            if (servicio.Propietario == turnosJugadores.Peek())
            {
                dpdwnJugadores.interactable = true;
            }
        }

        public void MostrarCartaFerrocarril(Ferrocarril ferrocarril)
        {
            CartaPropiedad.SetActive(false);
            CartaServicio.SetActive(false);
            CartaFerrocarril.SetActive(true);

            CartaFerrocarril.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Ferrocarril " + ferrocarril.Nombre;
            CartaFerrocarril.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Valor: $" + ferrocarril.Valor.ToString();
            CartaFerrocarril.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "Alquiler: $" + ferrocarril.Renta.ToString();
            CartaFerrocarril.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = "Con 2 Ferrocarriles: $" + ferrocarril.RentaConFerrocarriles[1].ToString();
            CartaFerrocarril.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = "Con 3 Ferrocarriles: $" + ferrocarril.RentaConFerrocarriles[2].ToString();
            CartaFerrocarril.transform.GetChild(5).GetComponent<TextMeshProUGUI>().text = "Con 4 Ferrocarriles: $" + ferrocarril.RentaConFerrocarriles[3].ToString();
            CartaFerrocarril.transform.GetChild(6).GetComponent<TextMeshProUGUI>().text = "Hipoteca: $" + ferrocarril.Hipoteca.ToString();


            if (ferrocarril.Propietario == turnosJugadores.Peek())
            {
                dpdwnJugadores.interactable = true;
            }
        }

        #endregion

        #region Botones Usuario
        public void TirarDados()
        {
            turnosJugadores.Peek().TirarDados();
            MostrarTirada(turnosJugadores.Peek());

            EvaluarEncarcelamiento(turnosJugadores);

            if (movimientoJugador)
            {
                movimientoJugador = false;
                MoverJugador(nivel.Tablero, turnosJugadores.Peek(), turnosJugadores.Peek().SumaUltimosDados);
                InstanciadorPrefabs.instancia.TrasladarFicha(nivel.Tablero, jugadores, turnosJugadores.Peek());
                EvaluarTiradaDoble(turnosJugadores);
            }

            if (!movimientoJugador)
            {
                EvaluarTerrenoEnVenta();
            }            
        }

        private void EvaluarTerrenoEnVenta()
        {
            Jugador jugadorAux = turnosJugadores.Peek();

            EvaluarTerreno();

            switch (tipoTerrenoCasillero)
            {
                case "Propiedad":
                    Propiedad propiedadAux = nivel.Tablero.damePropiedad(jugadorAux.Posicion);
                    if (propiedadAux.EnVenta)
                    {
                        if (propiedadAux.Valor <= jugadorAux.Dinero)
                        {
                            botonComprarTerreno.interactable = true;
                            botonIniciarSubasta.interactable = true;
                        }
                        else
                        {
                            botonComprarTerreno.interactable = false;
                            botonIniciarSubasta.interactable = true;
                        }
                    }
                    else
                    {
                        if (propiedadAux.Hipotecado || propiedadAux.Propietario == jugadorAux)
                        {
                            botonPagarRenta.interactable = false;
                            botonTerminarTurno.interactable = true;
                        }
                        else
                        {
                            if(!(jugadorAux.Dinero < propiedadAux.CalcularRenta()))
                            {
                                botonPagarRenta.interactable = true;
                            }
                            else
                            {
                                botonPagarRenta.interactable = false;
                                botonDeclararBancarrota.interactable = true;
                            }                            
                        }
                    }
                    break;
                case "Ferrocarril":
                    Ferrocarril ferrocarrilAux = nivel.Tablero.dameFerrocarril(jugadorAux.Posicion);
                    if (ferrocarrilAux.EnVenta)
                    {
                        if (ferrocarrilAux.Valor <= jugadorAux.Dinero)
                        {
                            botonComprarTerreno.interactable = true;
                            botonIniciarSubasta.interactable = true;
                        }
                        else
                        {
                            botonComprarTerreno.interactable = false;
                            botonIniciarSubasta.interactable = true;
                        }
                    }
                    else
                    {
                        if (ferrocarrilAux.Hipotecado || ferrocarrilAux.Propietario == jugadorAux)
                        {
                            botonPagarRenta.interactable = false;
                            botonTerminarTurno.interactable = true;
                        }
                        else
                        {
                            if (!(jugadorAux.Dinero < ferrocarrilAux.CalcularRenta()))
                            {
                                botonPagarRenta.interactable = true;
                            }
                            else
                            {
                                botonPagarRenta.interactable = false;
                                botonDeclararBancarrota.interactable = true;
                            }
                        }
                    }
                    break;
                case "Servicio":
                    Servicio servicioAux = nivel.Tablero.dameServicio(jugadorAux.Posicion);
                    if (servicioAux.EnVenta)
                    {
                        if (servicioAux.Valor <= jugadorAux.Dinero)
                        {
                            botonComprarTerreno.interactable = true;
                            botonIniciarSubasta.interactable = true;
                        }
                        else
                        {
                            botonComprarTerreno.interactable = false;
                            botonIniciarSubasta.interactable = true;
                        }
                    }
                    else
                    {
                        if (servicioAux.Hipotecado || servicioAux.Propietario == jugadorAux)
                        {
                            botonPagarRenta.interactable = false;
                            botonTerminarTurno.interactable = true;
                        }
                        else
                        {
                            if (!(jugadorAux.Dinero < servicioAux.CalcularRenta()))
                            {
                                botonPagarRenta.interactable = true;
                            }
                            else
                            {
                                botonPagarRenta.interactable = false;
                                botonDeclararBancarrota.interactable = true;
                            }
                        }
                    }
                    break;
                default:
                    botonTerminarTurno.interactable = true;
                    break;
            }
        }

        private void EvaluarTerreno()
        {
            Jugador jugadorAux = turnosJugadores.Peek();

            if (nivel.Tablero.damePropiedad(jugadorAux.Posicion) != null)
            {
                botonMostrarTarjeta.interactable = true;
                tipoTerrenoCasillero = "Propiedad";
            }

            else if (nivel.Tablero.dameFerrocarril(jugadorAux.Posicion) != null)
            {
                botonMostrarTarjeta.interactable = true;
                tipoTerrenoCasillero = "Ferrocarril";
            }

            else if (nivel.Tablero.dameServicio(jugadorAux.Posicion) != null)
            {
                botonMostrarTarjeta.interactable = true;
                tipoTerrenoCasillero = "Servicio";
            }

            else
            {
                tipoTerrenoCasillero = "";
                botonMostrarTarjeta.interactable = false;
                botonComprarTerreno.interactable = false;
                botonIniciarSubasta.interactable = false;
            }
        }

        public void ComprarTerreno()
        {
            botonComprarTerreno.interactable = false;
            botonIniciarSubasta.interactable = false;

            Jugador jugadorAux = turnosJugadores.Peek();

            if (nivel.Tablero.damePropiedad(jugadorAux.Posicion) != null)
            {
                Propiedad propiedadAux = nivel.Tablero.damePropiedad(jugadorAux.Posicion);

                jugadorAux.ComprarPropiedad(propiedadAux);
                Debug.Log("Compraste " + propiedadAux.Nombre + " por $" + propiedadAux.Valor);
                Debug.Log("Te quedan: $" + jugadorAux.Dinero);
            }

            else if (nivel.Tablero.dameFerrocarril(jugadorAux.Posicion) != null)
            {
                Ferrocarril ferrocarrilAux = nivel.Tablero.dameFerrocarril(jugadorAux.Posicion);

                jugadorAux.ComprarFerrocarril(ferrocarrilAux);
                Debug.Log("Compraste " + ferrocarrilAux.Nombre + " por $" + ferrocarrilAux.Valor);
                Debug.Log("Te quedan: $" + jugadorAux.Dinero);
            }

            else if (nivel.Tablero.dameServicio(jugadorAux.Posicion) != null)
            {
                Servicio servicioAux = nivel.Tablero.dameServicio(jugadorAux.Posicion);

                jugadorAux.ComprarServicio(servicioAux);
                Debug.Log("Compraste " + servicioAux.Nombre + " por $" + servicioAux.Valor);
                Debug.Log("Te quedan: $" + jugadorAux.Dinero);
            }

            botonTerminarTurno.interactable = true;
            ConsultaTerrenosAdquiridos();
        }

        public void PagarRenta()
        {
            botonPagarRenta.interactable = false;

            Jugador jugadorAux = turnosJugadores.Peek();

            if (nivel.Tablero.damePropiedad(jugadorAux.Posicion) != null)
            {
                Propiedad propiedadAux = nivel.Tablero.damePropiedad(jugadorAux.Posicion);

                propiedadAux.CobrarRenta(jugadorAux);
                Debug.Log("Le pagaste $" + propiedadAux.CalcularRenta() + " a " + propiedadAux.Propietario.Color);
                Debug.Log("Te quedan: $" + jugadorAux.Dinero);

            }

            else if (nivel.Tablero.dameFerrocarril(jugadorAux.Posicion) != null)
            {
                Ferrocarril ferrocarrilAux = nivel.Tablero.dameFerrocarril(jugadorAux.Posicion);

                ferrocarrilAux.CobrarRenta(jugadorAux);
                Debug.Log("Le pagaste $" + ferrocarrilAux.CalcularRenta() + " a " + ferrocarrilAux.Propietario.Color);
                Debug.Log("Te quedan: $" + jugadorAux.Dinero);
            }

            else if (nivel.Tablero.dameServicio(jugadorAux.Posicion) != null)
            {
                Servicio servicioAux = nivel.Tablero.dameServicio(jugadorAux.Posicion);

                servicioAux.AdquirirUsuario(jugadorAux);
                servicioAux.CobrarRenta(jugadorAux);
                Debug.Log("Le pagaste $" + servicioAux.CalcularRenta() + " a " + servicioAux.Propietario.Color);
                Debug.Log("Te quedan: $" + jugadorAux.Dinero);
            }

            botonTerminarTurno.interactable = true;
        }

        public void TerminarTurno()
        {
            InsertarJugadorAlFondo(turnosJugadores, turnosJugadores.Pop());
            MostrarJugador(turnosJugadores.Peek());
            botonTirarDados.interactable = true;
            botonTerminarTurno.interactable = false;
            movimientoJugador = true;
            TextMeshProUGUI txtValor = botonValorSubasta.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            valorSubasta = Int32.Parse(txtValor.text);

            OcultarTarjeta();

            EvaluarTerreno();     

            ConsultaTerrenosAdquiridos();

            int indice = 0;
            foreach (TextMeshProUGUI txtMesh in puntajes)
            {
                txtMesh.color = InstanciadorPrefabs.instancia.damelstMaterialesMonopoly().Find(x => x.name == jugadores[indice].Color).color;
                txtMesh.text = jugadores[indice].Color + " - $" + jugadores[indice].Dinero;
                if (jugadores[indice] == turnosJugadores.Peek()) txtMesh.fontStyle = FontStyles.Underline;
                else txtMesh.fontStyle = FontStyles.Normal;
                indice++;
            }
        }

        

        #endregion

        #region Subasta
        public void Subasta()
        {
            botonComprarTerreno.interactable = false;
            botonIniciarSubasta.interactable = false;
            botonSubasta10Arriba.interactable = true;
            botonSubasta10Abajo.interactable = false;
            botonSalirSubasta.interactable = true;
            botonValorSubasta.interactable = true;

            subastador = turnosJugadores.Peek();

            foreach (Jugador jugador in turnosJugadores)
            {
                jugador.EnSubasta = true;
            }
        }

        public void SalirSubasta()
        {
            botonSalirSubasta.interactable = false;
            turnosJugadores.Peek().EnSubasta = false;
            TerminarTurnoSubasta();

            int count = 0;
            foreach (Jugador jugador in turnosJugadores)
            {
                if (jugador.EnSubasta) count++;
            }

            while (!turnosJugadores.Peek().EnSubasta) TerminarTurnoSubasta();

            if (count == 1)
            {
                botonSubasta10Arriba.interactable = false;
                botonSubasta10Abajo.interactable = false;
                botonSalirSubasta.interactable = false;
                botonValorSubasta.interactable = true;
            }
            else
            {
                botonSalirSubasta.interactable = true;
                botonSubasta10Abajo.interactable = false;
                botonSubasta10Arriba.interactable = true;
            }
        }

        public void Subasta10Arriba()
        {
            botonValorSubasta.interactable = true;
            TextMeshProUGUI txtValor = botonValorSubasta.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            int valor = Int32.Parse(txtValor.text);
            valor += 10;
            txtValor.text = valor.ToString();
            if (!botonSubasta10Abajo.interactable) botonSubasta10Abajo.interactable = true;
        }

        public void Subasta10Abajo()
        {
            TextMeshProUGUI txtValor = botonValorSubasta.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            int valor = Int32.Parse(txtValor.text);
            valor -= 10;
            txtValor.text = valor.ToString();
            if (valor <= valorSubasta)
            {
                botonSubasta10Abajo.interactable = false;
                botonValorSubasta.interactable = false;
            }
        }

        public void ParticiparSubasta()
        {
            int count = 0;
            foreach (Jugador jugador in turnosJugadores)
            {
                if (jugador.EnSubasta) count++;
            }

            while (!turnosJugadores.Peek().EnSubasta) TerminarTurnoSubasta();

            if (count == 1)
            {
                Jugador jugadorAux = turnosJugadores.Peek();

                if (nivel.Tablero.damePropiedad(subastador.Posicion) != null)
                {
                    Propiedad propiedadAux = nivel.Tablero.damePropiedad(subastador.Posicion);

                    jugadorAux.ComprarPropiedad(propiedadAux);
                    Debug.Log("Compraste " + propiedadAux.Nombre + " por $" + propiedadAux.Valor);
                    Debug.Log("Te quedan: $" + jugadorAux.Dinero);
                }

                else if (nivel.Tablero.dameFerrocarril(subastador.Posicion) != null)
                {
                    Ferrocarril ferrocarrilAux = nivel.Tablero.dameFerrocarril(subastador.Posicion);

                    jugadorAux.ComprarFerrocarril(ferrocarrilAux);
                    Debug.Log("Compraste " + ferrocarrilAux.Nombre + " por $" + ferrocarrilAux.Valor);
                    Debug.Log("Te quedan: $" + jugadorAux.Dinero);
                }

                else if (nivel.Tablero.dameServicio(subastador.Posicion) != null)
                {
                    Servicio servicioAux = nivel.Tablero.dameServicio(subastador.Posicion);

                    jugadorAux.ComprarServicio(servicioAux);
                    Debug.Log("Compraste " + servicioAux.Nombre + " por $" + servicioAux.Valor);
                    Debug.Log("Te quedan: $" + jugadorAux.Dinero);
                }

                jugadorAux.EnSubasta = false;

                while (turnosJugadores.Peek() != subastador) TerminarTurnoSubasta();

                botonValorSubasta.interactable = false;
                botonTerminarTurno.interactable = true;

                valorSubasta = 10;
                TextMeshProUGUI txtValor = botonValorSubasta.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
                txtValor.text = valorSubasta.ToString();
            }
            else
            {
                TextMeshProUGUI txtValor = botonValorSubasta.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
                valorSubasta = Int32.Parse(txtValor.text);
                TerminarTurnoSubasta();
            }
        }

        private void TerminarTurnoSubasta()
        {
            botonSubasta10Abajo.interactable = false;
            botonValorSubasta.interactable = false;
            InsertarJugadorAlFondo(turnosJugadores, turnosJugadores.Pop());
            MostrarJugador(turnosJugadores.Peek());
            botonTerminarTurno.interactable = false;
            movimientoJugador = true;
        }
        #endregion

        #region Jugador: Movimiento y Tiradas
        private void AgregarJugadores(List<Jugador> jugadores)
        {
            jugadores.Add(new Jugador("Ana", 1500, new Vector2(0, 0), "Izquierda", "Azul"));
            jugadores.Add(new Jugador("Bruno", 1500, new Vector2(0, 0), "Izquierda", "Rojo"));
            jugadores.Add(new Jugador("Carla", 1500, new Vector2(0, 0), "Izquierda", "Verde"));
            jugadores.Add(new Jugador("Daniel", 1500, new Vector2(0, 0), "Izquierda", "Amarillo"));
            jugadores.Add(new Jugador("Estela", 1500, new Vector2(0, 0), "Izquierda", "Celeste"));
            jugadores.Add(new Jugador("Facundo", 1500, new Vector2(0, 0), "Izquierda", "Magenta"));
        }

        private void MostrarJugador(Jugador jugador)
        {
            Debug.Log("Color: " + jugador.Color + " -- " + "Dinero: " + jugador.Dinero);
        }

        #region Tirada
        private void MostrarTirada(Jugador jugador)
        {
            Debug.Log(jugador.Color + ": " + jugador.ultimosDados[0] + " + " + jugador.ultimosDados[1] + " = " + jugador.SumaUltimosDados);
            Debug.Log("Tiradas dobles: " + jugador.TiradasDobles);
        }

        private void EvaluarTiradaDoble(Stack<Jugador> turnosJugadores)
        {
            if (turnosJugadores.Peek().TiradasDobles > 0)
            {
                movimientoJugador = true;
                botonTirarDados.interactable = true;
                Debug.Log("Puedes avanzar nuevamente");
            }
            else
            {
                movimientoJugador = false;
                botonTirarDados.interactable = false;
            }
        }
        #endregion

        private void MoverJugador(Tablero tablero, Jugador jugador, int valor)
        {
            switch (jugador.Direccion)
            {
                case "Izquierda":
                    jugador.Posicion = new Vector2(jugador.Posicion.x + valor, jugador.Posicion.y);
                    if (jugador.Posicion.x >= tablero.casilleros.GetLength(0) - 1)
                    {
                        jugador.Direccion = "Arriba";
                        valor = (int)(jugador.Posicion.x - (tablero.casilleros.GetLength(0) - 1));

                        jugador.Posicion = new Vector2(tablero.casilleros.GetLength(0) - 1, jugador.Posicion.y);
                        MoverJugador(nivel.Tablero, turnosJugadores.Peek(), valor);
                    }
                    break;

                case "Arriba":
                    jugador.Posicion = new Vector2(jugador.Posicion.x, jugador.Posicion.y + valor);
                    if (jugador.Posicion.y >= tablero.casilleros.GetLength(1) - 1)
                    {
                        jugador.Direccion = "Derecha";
                        valor = (int)(jugador.Posicion.y - (tablero.casilleros.GetLength(1) - 1));

                        jugador.Posicion = new Vector2(jugador.Posicion.x, tablero.casilleros.GetLength(1) - 1);
                        MoverJugador(nivel.Tablero, turnosJugadores.Peek(), valor);
                    }
                    break;

                case "Derecha":
                    jugador.Posicion = new Vector2(jugador.Posicion.x - valor, jugador.Posicion.y);
                    if (jugador.Posicion.x <= 0)
                    {
                        if (jugador.Posicion.x == 0)
                        {
                            IrALaCarcel(jugador);
                            break;
                        }
                        jugador.Direccion = "Abajo";
                        valor = (int)(-jugador.Posicion.x);

                        jugador.Posicion = new Vector2(0, jugador.Posicion.y);
                        MoverJugador(nivel.Tablero, turnosJugadores.Peek(), valor);
                    }
                    break;

                case "Abajo":
                    jugador.Posicion = new Vector2(jugador.Posicion.x, jugador.Posicion.y - valor);
                    if (jugador.Posicion.y <= 0)
                    {
                        PasarPorLaSalida(jugador);
                        jugador.Direccion = "Izquierda";
                        valor = (int)(-jugador.Posicion.y);

                        jugador.Posicion = new Vector2(jugador.Posicion.x, 0);
                        MoverJugador(nivel.Tablero, turnosJugadores.Peek(), valor);
                    }
                    break;

                default:
                    break;
            }
        }

        #endregion

        #region Turnos: Definicion y Administracion
        private void DefinirTurnos(List<Jugador> jugadores, Stack<Jugador> turnosJugadores)
        {
            foreach (Jugador jugador in jugadores)
            {
                jugador.TirarDados();
                jugador.TiradasDobles = 0;
                MostrarTirada(jugador);    
            }

            jugadores = jugadores.OrderBy(x => x.SumaUltimosDados).ToList();

            foreach (Jugador jugador in jugadores)
            {
                turnosJugadores.Push(jugador);
            }

            MostrarTurnos(turnosJugadores);
        }

        private void InsertarJugadorAlFondo(Stack<Jugador> turnosJugadores, Jugador jugador)
        {
            if (turnosJugadores.Count == 0)
            {
                turnosJugadores.Push(jugador);
                return;
            }

            Jugador tmpJugador = turnosJugadores.Peek();
            turnosJugadores.Pop();
            InsertarJugadorAlFondo(turnosJugadores, jugador);
            turnosJugadores.Push(tmpJugador);
            return;
        }

        private void MostrarTurnos(Stack<Jugador> turnosJugadores)
        {
            int index = 1;
            foreach (Jugador jugador in turnosJugadores)
            {
                Debug.Log("Turno " + index + ": " + jugador.Color);
                index++;
            }
            Debug.Log("-----------------------------------------");
        }
        #endregion

        #region Carcel: Evaluacion y Administracion
        private void EvaluarEncarcelamiento(Stack<Jugador> turnosJugadores)
        {
            if (turnosJugadores.Peek().Preso)
            {
                movimientoJugador = false;
                turnosJugadores.Peek().DiasPreso++;
                Debug.Log("Dias Preso :" + turnosJugadores.Peek().DiasPreso);

                if (turnosJugadores.Peek().TiradasDobles > 0)
                {
                    turnosJugadores.Peek().DiasPreso = 0;
                    turnosJugadores.Peek().Preso = false;
                    movimientoJugador = true;
                }
                else if (turnosJugadores.Peek().DiasPreso == 3)
                {
                    turnosJugadores.Peek().DiasPreso = 0;
                    PagarFianza(turnosJugadores.Peek(), 50);
                    movimientoJugador = true;
                }
                else
                {
                    botonTirarDados.interactable = false;
                    botonTerminarTurno.interactable = true;
                }
            }
            else if (turnosJugadores.Peek().TiradasDobles == 3)
            {
                Debug.Log("Vas a la carcel por exceso de velocidad");
                IrALaCarcel(turnosJugadores.Peek());
            }
        }

        private void IrALaCarcel(Jugador jugador)
        {
            movimientoJugador = false;
            botonTirarDados.interactable = false;
            Debug.Log("Vas a la carcel directamente sin pasar por la salida y sin cobrar $200");
            jugador.Posicion = new Vector2(nivel.Tablero.casilleros.GetLength(1) - 1, 0);
            jugador.Direccion = "Arriba";
            jugador.Preso = true;
            jugador.TiradasDobles = 0;
            InstanciadorPrefabs.instancia.TrasladarFicha(nivel.Tablero, jugadores, turnosJugadores.Peek());
        }

        private void PagarFianza(Jugador jugador, int fianza)
        {
            jugador.QuitarDinero(fianza);
            jugador.Preso = false;
            Debug.Log("Pagas la fianza, vuelves a ser libre");
        }
        #endregion

        private void PasarPorLaSalida(Jugador jugador)
        {
            jugador.AdquirirDinero(200);
            Debug.Log("Cobraste 200");
            MostrarJugador(jugador);
        }

        private void CargarNivel(string nombre)
        {
            nivel = MonopolyLevelManager.instancia.dameNivel(nombre);
            InstanciadorPrefabs.instancia.GraficarCasilleros(nivel.Tablero, casillero);
            InstanciadorPrefabs.instancia.GraficarCasillerosEsquina(nivel.Tablero, casilleroEsquina);
            InstanciadorPrefabs.instancia.GraficarCasillerosPropiedad(nivel.Tablero, casilleroPropiedad, terrenoNombre, terrenoValor);
            InstanciadorPrefabs.instancia.GraficarCasillerosFerrocarril(nivel.Tablero, casilleroFerrocarril, terrenoNombre, terrenoValor);
            InstanciadorPrefabs.instancia.GraficarCasillerosServicio(nivel.Tablero, casilleroServicio);
            InstanciadorPrefabs.instancia.GraficarFichasJugadores(nivel.Tablero, jugadores, fichaJugador);
        }

    }
}