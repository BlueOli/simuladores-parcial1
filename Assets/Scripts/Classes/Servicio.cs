using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Servicio: Terreno
{
    public Jugador Usuario { get; set; }

    public Servicio(string nombre, int valor, int renta)
    {
        Nombre = nombre;
        Valor = valor;
        Renta = renta;
        Hipoteca = CalcularHipoteca();
        Hipotecado = false;
        EnVenta = true;
    }

    public void AdquirirUsuario(Jugador jugador)
    {
        Usuario = jugador;
        Renta = CalcularRenta();
    }

    public override int CalcularRenta()
    {
        if (Propietario.Servicios.Count == 2)
        {
            return Usuario.SumaUltimosDados * 10;
        }
        else
        {
            return Usuario.SumaUltimosDados * 4;
        }
    }
}
