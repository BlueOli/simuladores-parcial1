using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Propiedad: Terreno
{
    public string Color { get; set; }
    public int [] RentaConCasas { get; set; }
    public int RentaConHotel { get; set; }
    public int CostoPorCasa { get; set; }
    public int CostoPorHotel { get; set; }
    public int Casas { get; set; }
    public int Hoteles { get; set; }

    public Propiedad(string nombre, int valor, int renta, int [] rentaConCasas,
        int rentaConHotel, int costoPorCasa, int costoPorHotel)
    {
        Nombre = nombre;
        Valor = valor;
        Renta = renta;
        RentaConCasas = rentaConCasas;
        RentaConHotel = rentaConHotel;
        CostoPorCasa = costoPorCasa;
        CostoPorHotel = costoPorHotel;
        Casas = 0;
        Hoteles = 0;
        Hipoteca = CalcularHipoteca();
        Hipotecado = false;
        EnVenta = true;
    }

    public override int CalcularRenta()
    {
        if(Hoteles > 0)
        {
            return RentaConHotel;
        }
        else if (Casas > 0)
        {
            return RentaConCasas[Casas - 1];
        }
        else
        {
            int count = 0;
            foreach (Propiedad propiedad in this.Propietario.Propiedades)
            {
                if (this.Color == propiedad.Color) count++;
            }

            if (count == 3 || ((this.Color == "Marron" || this.Color == "Azul") && count == 2))
            {
                return this.Renta * 2;
            }
            else
            {
                return Renta;
            }
        }
    }
}
