﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Classes
{
    class Carta
    {
        public string Tipo { get; set; }
        public string Info { get; set; }
        public int IdConsigna { get; set; }
        public bool Cobrar200 { get; set; }
        public bool PagarDoble { get; set; }


        public Carta(string tipo, string info, int idConsigna)
        {
            Tipo = tipo;
            Info = info;
            IdConsigna = idConsigna;
        }

        /*
         * Hacer avanzar
         * Hacer avanzar y cobrar 200
         * Ferrocarril mas cercano, comprar o pagar el doble
         * Servicio mas cercano, comprar o pagar *10
         * Cobrar dinero
         * Salir libre de la carcel
         * Ir a la carcel
         * Pagar por cada casa y hotel
         * Pagar multa
         * Cada jugador te paga
         * Pagar a cada jugador
         */

        public void CumplirConsigna(int idConsigna)
        {
            switch (idConsigna)
            {


                default:
                    break;
            }
        }

        public void AvanzarHasta22(Tablero tablero, Jugador jugador)
        {
            Cobrar200 = false;
            while (jugador.Posicion == tablero.damePosicionPropiedad("Propiedad 22"))
            {
                MoverJugador(tablero, jugador, 1);
            }
        }

        public void AvanzarHastaSalida(Tablero tablero, Jugador jugador)
        {
            Cobrar200 = true;
            while (jugador.Posicion == new Vector2(0,0))
            {
                MoverJugador(tablero, jugador, 1);
            }
        }

        public void AvanzarHasta14(Tablero tablero, Jugador jugador)
        {
            Cobrar200 =true;
            while (jugador.Posicion == tablero.damePosicionPropiedad("Propiedad 14"))
            {
                MoverJugador(tablero, jugador, 1);
            }
        }

        public void AvanzarHasta6(Tablero tablero, Jugador jugador)
        {
            Cobrar200 = true;
            while (jugador.Posicion == tablero.damePosicionPropiedad("Propiedad 6"))
            {
                MoverJugador(tablero, jugador, 1);
            }
        }

        public void AvanzarHastaFerrocarril(Tablero tablero, Jugador jugador)
        {
            Cobrar200 = false;
            while (tablero.ferrocarriles[(int)jugador.Posicion.x, (int)jugador.Posicion.y] != null)
            {
                MoverJugador(tablero, jugador, 1);
            }
        }







        private void MoverJugador(Tablero tablero, Jugador jugador, int valor)
        {
            switch (jugador.Direccion)
            {
                case "Izquierda":
                    jugador.Posicion = new Vector2(jugador.Posicion.x + valor, jugador.Posicion.y);
                    if (jugador.Posicion.x >= tablero.casilleros.GetLength(0) - 1)
                    {
                        jugador.Direccion = "Arriba";
                        valor = (int)(jugador.Posicion.x - (tablero.casilleros.GetLength(0) - 1));

                        jugador.Posicion = new Vector2(tablero.casilleros.GetLength(0) - 1, jugador.Posicion.y);
                        MoverJugador(tablero, jugador, valor);
                    }
                    break;

                case "Arriba":
                    jugador.Posicion = new Vector2(jugador.Posicion.x, jugador.Posicion.y + valor);
                    if (jugador.Posicion.y >= tablero.casilleros.GetLength(1) - 1)
                    {
                        jugador.Direccion = "Derecha";
                        valor = (int)(jugador.Posicion.y - (tablero.casilleros.GetLength(1) - 1));

                        jugador.Posicion = new Vector2(jugador.Posicion.x, tablero.casilleros.GetLength(1) - 1);
                        MoverJugador(tablero, jugador, valor);
                    }
                    break;

                case "Derecha":
                    jugador.Posicion = new Vector2(jugador.Posicion.x - valor, jugador.Posicion.y);
                    if (jugador.Posicion.x <= 0)
                    {
                        jugador.Direccion = "Abajo";
                        valor = (int)(-jugador.Posicion.x);

                        jugador.Posicion = new Vector2(0, jugador.Posicion.y);
                        MoverJugador(tablero, jugador, valor);
                    }
                    break;

                case "Abajo":
                    jugador.Posicion = new Vector2(jugador.Posicion.x, jugador.Posicion.y - valor);
                    if (jugador.Posicion.y <= 0)
                    {
                        if (Cobrar200)
                        {
                            PasarPorLaSalida(jugador);
                        }
                        jugador.Direccion = "Izquierda";
                        valor = (int)(-jugador.Posicion.y);

                        jugador.Posicion = new Vector2(jugador.Posicion.x, 0);
                        MoverJugador(tablero, jugador, valor);
                    }
                    break;

                default:
                    break;
            }
        }

        private void PasarPorLaSalida(Jugador jugador)
        {
            jugador.AdquirirDinero(200);
            Debug.Log("Cobraste 200");
        }


    }
}
