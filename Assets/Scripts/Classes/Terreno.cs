using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Terreno
{
    public string Nombre { get; set; }
    public int Valor { get; set; }
    public int Hipoteca { get; set; }
    public bool EnVenta { get; set; }
    public bool Hipotecado { get; set; }
    public int Renta { get; set; }
    public Jugador Propietario { get; set; }

    public void CobrarRenta(Jugador visitante)
    {
        int valor = CalcularRenta();
        visitante.QuitarDinero(valor);
        Propietario.AdquirirDinero(valor);
    }

    public virtual int CalcularRenta()
    {
        return this.Renta;
    }

    public void Hipotecar()
    {
        Propietario.AdquirirDinero(Hipoteca);
        this.Hipotecado = true;
    }

    public virtual int CalcularHipoteca()
    {
        return this.Valor / 2;
    }

}
