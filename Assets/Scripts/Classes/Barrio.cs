using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrio
{
    public List<Propiedad> LstPropiedades { get; set; }
    public string Color { get; set; }

    public Barrio(string color)
    {
        Color = color;
        LstPropiedades = new List<Propiedad>();
    }

    public void AgregarPropiedad(Propiedad propiedad)
    {
        propiedad.Color = this.Color;
        LstPropiedades.Add(propiedad);
    }
}
