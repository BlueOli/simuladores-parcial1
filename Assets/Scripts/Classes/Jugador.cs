using System.Collections.Generic;
using UnityEngine;

public class Jugador
{
    public string Nombre { get; set; }

    public List<Propiedad> Propiedades { get; set; }

    public List<Servicio> Servicios { get; set; }

    public List<Ferrocarril> Ferrocarriles { get; set; }

    public Vector2 Posicion { get; set; }

    public int Dinero { get; set; }

    public string Color { get; set; }

    public int[] ultimosDados = { 0, 0 };

    public int SumaUltimosDados { get; set; }

    public string Direccion { get; set; }

    public bool Preso { get; set; }

    public int DiasPreso { get; set; }

    public int TiradasDobles { get; set; }

    public bool EnSubasta { get; set; }

    public Jugador(string nombre, int dinero, Vector2 posicion, string direccion, string color)
    {
        Nombre = nombre;
        Dinero = dinero;
        Propiedades = new List<Propiedad>();
        Servicios = new List<Servicio>();
        Ferrocarriles = new List<Ferrocarril>();
        Posicion = posicion;
        Direccion = direccion;
        Color = color;
        Preso = false;
        DiasPreso = 0;
        TiradasDobles = 0;
        EnSubasta = false;
    }

    public void AdquirirFerrocarril(Ferrocarril ferrocarril)
    {
        Ferrocarriles.Add(ferrocarril);
        ferrocarril.Propietario = this;
    }

    public bool ComprarFerrocarril(Ferrocarril ferrocarril)
    {
        if (Dinero >= ferrocarril.Valor)
        {
            QuitarDinero(ferrocarril.Valor);
            AdquirirFerrocarril(ferrocarril);
            ferrocarril.EnVenta = false;
            return true;
        }
        else
        {
            return false;
        }
    }

    public void AdquirirServicio(Servicio servicio)
    {
        Servicios.Add(servicio);
        servicio.Propietario = this;
    }

    public bool ComprarServicio(Servicio servicio)
    {
        if (Dinero >= servicio.Valor)
        {
            QuitarDinero(servicio.Valor);
            AdquirirServicio(servicio);
            servicio.EnVenta = false;
            return true;
        }
        else
        {
            return false;
        }
    }

    public void AdquirirPropiedad(Propiedad propiedad)
    {
        Propiedades.Add(propiedad);
        propiedad.Propietario = this;
    }

    public bool ComprarPropiedad(Propiedad propiedad)
    {
        if (Dinero >= propiedad.Valor)
        {
            QuitarDinero(propiedad.Valor);
            AdquirirPropiedad(propiedad);
            propiedad.EnVenta = false;
            return true;
        }
        else
        {
            return false;
        }
    }

    public void VenderPropiedad(Jugador comprador, int indice)
    {
        comprador.ComprarPropiedad(this.Propiedades[indice]);
        this.Propiedades.Remove(this.Propiedades[indice]);
    }

    public void VenderServicio(Jugador comprador, int indice)
    {
        comprador.ComprarServicio(this.Servicios[indice]);
        this.Servicios.Remove(this.Servicios[indice]);
    }

    public void VenderFerrocarril(Jugador comprador, int indice)
    {
        comprador.ComprarFerrocarril(this.Ferrocarriles[indice]);
        this.Ferrocarriles.Remove(this.Ferrocarriles[indice]);
    }

    public Propiedad damePropiedad(string nombre)
    {
        foreach (Propiedad propiedad in Propiedades)
        {
            if (propiedad.Nombre == nombre)
            {
                return propiedad;
            }
        }
        return null;
    }

    public void AdquirirDinero(int dinero)
    {
        Dinero += dinero;
    }

    public void QuitarDinero(int dinero)
    {
        Dinero -= dinero;
    }

    public void CambiarPosicion(Vector2 posicion)
    {
        Posicion = posicion;
    }

    public void TirarDados()
    {
        int dado = Random.Range(1, 7);
        ultimosDados[0] = dado;
        dado = Random.Range(1, 7);
        ultimosDados[1] = dado;
        SumaUltimosDados = ultimosDados[0] + ultimosDados[1];
        if (ultimosDados[0] == ultimosDados[1])
        {
            TiradasDobles++;
        }
        else
        {
            TiradasDobles = 0;
        }
    }

    public void DeclararQuiebra()
    {
        foreach (Propiedad p in Propiedades)
        {
            p.Propietario = null;
        }
        Propiedades.Clear();

        foreach (Servicio s in Servicios)
        {
            s.Propietario = null;
        }
        Servicios.Clear();

        foreach (Ferrocarril f in Ferrocarriles)
        {
            f.Propietario = null;
        }
        Ferrocarriles.Clear();

        Dinero = 0;
    }

    public void DeclararQuiebra(Jugador jugador)
    {
        foreach (Propiedad p in Propiedades)
        {
            jugador.AdquirirPropiedad(p);            
        }
        Propiedades.Clear();

        foreach (Servicio s in Servicios)
        {
            jugador.AdquirirServicio(s);
        }
        Servicios.Clear();

        foreach (Ferrocarril f in Ferrocarriles)
        {
            jugador.AdquirirFerrocarril(f);
        }
        Ferrocarriles.Clear();

        jugador.AdquirirDinero(Dinero);
        Dinero = 0;
    }
}
