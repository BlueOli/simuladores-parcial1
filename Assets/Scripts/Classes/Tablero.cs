using System.Collections.Generic;
using UnityEngine;

public class Tablero
{
    public GameObject[,] casilleros { get; set; }
    public Propiedad[,] propiedades { get; set; }

    public List<Barrio> barrios = new List<Barrio>();
    public Servicio[,] servicios { get; set; }
    public Ferrocarril[,] ferrocarriles { get; set; }

    public Tablero(int x, int y)
    {
        casilleros = new GameObject[x, y];
        propiedades = new Propiedad[x, y];
        servicios = new Servicio[x, y];
        ferrocarriles = new Ferrocarril[x, y];
    }

    #region Casilleros: Seteo y Obtencion
    public void setearObjeto(GameObject objeto, Vector2 posicion)
    {
        if (!(posicion.x < 0) || posicion.x < casilleros.GetLength(0)
            || !(posicion.y < 0) || posicion.y < casilleros.GetLength(1))
        {
            casilleros[(int)posicion.x, (int)posicion.y] = objeto;
        }
    }

    public void setearObjetos(GameObject objeto, List<Vector2> posiciones)
    {
        foreach (Vector2 posicion in posiciones)
        {
            if (!(posicion.x < 0) || posicion.x < casilleros.GetLength(0)
                || !(posicion.y < 0) || posicion.y < casilleros.GetLength(1))
            {
                casilleros[(int)posicion.x, (int)posicion.y] = objeto;
            }
        }
    }

    public GameObject dameObjeto(Vector2 origen)
    {
        if (origen.x < 0 || origen.x >= casilleros.GetLength(0)
            || origen.y < 0 || origen.y >= casilleros.GetLength(1))
        {
            return null;
        }
        else
        {
            return casilleros[(int)origen.x, (int)origen.y];
        }
    }
    public List<Vector2> damePosicionesObjetos(string nombre)
    {
        List<Vector2> lstPosiciones = new List<Vector2>();

        for (int i = 0; i < casilleros.GetLength(0); i++)
        {
            for (int j = 0; j < casilleros.GetLength(1); j++)
            {
                if ((dameObjeto(new Vector2(i, j)) != null) && (dameObjeto(new Vector2(i, j)).name == nombre))
                {
                    lstPosiciones.Add(new Vector2(i, j));
                }
            }
        }

        return lstPosiciones;
    }

    public Vector2 damePosicionObjeto(string nombre)
    {
        List<Vector2> lstPosicionesObjetos = damePosicionesObjetos(nombre);
        return lstPosicionesObjetos[0];
    }

    #endregion

    #region Propiedades y Barrios: Seteo y Obtencion

    public void agregarBarrio(Barrio barrio)
    {
        barrios.Add(barrio);
    }

    public Barrio dameBarrio(string color)
    {
        foreach(Barrio b in barrios)
        {
            if(b.Color == color) return b;
        }
        return null;
    }
    public List<Barrio> dameLstBarrios()
    {
        return barrios;
    }

    public void setearPropiedad(Propiedad propiedad, Vector2 posicion)
    {
        if (!(posicion.x < 0) || posicion.x < propiedades.GetLength(0)
            || !(posicion.y < 0) || posicion.y < propiedades.GetLength(1))
        {
            propiedades[(int)posicion.x, (int)posicion.y] = propiedad;
        }
    }

    public Propiedad damePropiedad(Vector2 origen)
    {
        if (origen.x < 0 || origen.x >= propiedades.GetLength(0)
            || origen.y < 0 || origen.y >= propiedades.GetLength(1))
        {
            return null;
        }
        else
        {
            return propiedades[(int)origen.x, (int)origen.y];
        }
    }

    public Vector2 damePosicionPropiedad(string nombre)
    {
        for(int i = 0; i < propiedades.GetLength(0); i++)
        {
            for(int j = 0; j < propiedades.GetLength(1); j++)
            {
                if (propiedades[i, j].Nombre == nombre) return new Vector2(i, j);
            }
        }
        return new Vector2();
    }
    #endregion

    #region Servicios: Seteo y Obtencion
    public void setearServicio(Servicio servicio, Vector2 posicion)
    {
        if (!(posicion.x < 0) || posicion.x < servicios.GetLength(0)
            || !(posicion.y < 0) || posicion.y < servicios.GetLength(1))
        {
            servicios[(int)posicion.x, (int)posicion.y] = servicio;
        }
    }

    public Servicio dameServicio(Vector2 origen)
    {
        if (origen.x < 0 || origen.x >= servicios.GetLength(0)
            || origen.y < 0 || origen.y >= servicios.GetLength(1))
        {
            return null;
        }
        else
        {
            return servicios[(int)origen.x, (int)origen.y];
        }
    }
    #endregion

    #region Ferrocarriles: Seteo y Obtencion
    public void setearFerrocarril(Ferrocarril ferrocarril, Vector2 posicion)
    {
        if (!(posicion.x < 0) || posicion.x < ferrocarriles.GetLength(0)
            || !(posicion.y < 0) || posicion.y < ferrocarriles.GetLength(1))
        {
            ferrocarriles[(int)posicion.x, (int)posicion.y] = ferrocarril;
        }
    }

    public Ferrocarril dameFerrocarril(Vector2 origen)
    {
        if (origen.x < 0 || origen.x >= ferrocarriles.GetLength(0)
            || origen.y < 0 || origen.y >= ferrocarriles.GetLength(1))
        {
            return null;
        }
        else
        {
            return ferrocarriles[(int)origen.x, (int)origen.y];
        }
    }
    #endregion

    #region Fortuna



    #endregion

}
