using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ferrocarril: Terreno
{
    public int[] RentaConFerrocarriles { get; set; }

    public Ferrocarril(string nombre, int valor, int renta, int[] rentaConFerrocarriles)
    {
        Nombre = nombre;
        Valor = valor;
        Renta = renta;
        RentaConFerrocarriles = rentaConFerrocarriles;
        Hipoteca = CalcularHipoteca();
        Hipotecado = false;
        EnVenta = true;
    }

    public override int CalcularRenta()
    {
        return RentaConFerrocarriles[Propietario.Ferrocarriles.Count - 1];
    }
}
