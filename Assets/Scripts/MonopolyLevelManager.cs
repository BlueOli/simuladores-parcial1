﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Classes
{
    public class MonopolyLevelManager : MonoBehaviour
    {
        public GameObject casillero;
        public GameObject casilleroEsquina;
        public GameObject casilleroPropiedad;
        public GameObject casilleroFerrocarril;
        public GameObject casilleroServicio;
        //public GameObject casilleroFortuna;
        //public GameObject casilleroArcaComunal;
        public GameObject propiedadNombre;
        public GameObject propiedadValor;
        public GameObject fichaJugador;

        public static MonopolyLevelManager instancia;

        void Awake()
        {
            if (instancia == null)
            {
                instancia = this;
            }
            else
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
        }

        public List<GameObject> damelstPrefabsMonopoly()
        {
            List<GameObject> lstPrefabsMonopoly = new List<GameObject>();
            lstPrefabsMonopoly.Add(casillero);
            lstPrefabsMonopoly.Add(casilleroEsquina);
            lstPrefabsMonopoly.Add(casilleroPropiedad);
            lstPrefabsMonopoly.Add(casilleroFerrocarril);
            lstPrefabsMonopoly.Add(casilleroServicio);
            //lstPrefabsMonopoly.Add(casilleroFortuna);
            //lstPrefabsMonopoly.Add(casilleroArcaComunal);
            lstPrefabsMonopoly.Add(propiedadNombre);
            lstPrefabsMonopoly.Add(propiedadValor);
            lstPrefabsMonopoly.Add(fichaJugador);

            return lstPrefabsMonopoly;
        }

        private Tablero dameTablero(int x, int y)
        {
            Tablero tablero = new Tablero(x, y);

            for (int i = 0; i < tablero.casilleros.GetLength(0); i++)
            {
                for (int j = 0; j < tablero.casilleros.GetLength(1); j++)
                {
                    if ((i==0 || i == tablero.casilleros.GetLength(0) - 1)
                        && (j == 0 || j == tablero.casilleros.GetLength(1) - 1))
                    {
                        tablero.setearObjeto(casilleroEsquina, new Vector2(i, j));
                    }
                    else if (i == 0 || i == tablero.casilleros.GetLength(0) - 1)
                    {
                        tablero.setearObjeto(casillero, new Vector2(i, j));
                    }
                    else
                    {
                        if(j == 0 || j == tablero.casilleros.GetLength(1) - 1)
                        {
                            tablero.setearObjeto(casillero, new Vector2(i, j));
                        }
                    }
                }
            }

            return tablero;
        }

        public Nivel dameNivel(string nombre)
        {
            return MonopolyLevelManager.instancia.dameNiveles().Find(x => x.Nombre == nombre);
        }

        public List<Nivel> dameNiveles()
        {
            List<Nivel> lstNiveles = new List<Nivel>();
            lstNiveles.Add(new Nivel("Nivel 1", MonopolyLevelManager.instancia.dameTableroNivel1()));
            return lstNiveles;
        }

        public Tablero dameTableroNivel1()
        {
            Tablero tablero = MonopolyLevelManager.instancia.dameTablero(11, 11);

            CargarPropiedades(tablero);

            CargarServicios(tablero);

            CargarFerrocarriles(tablero);

            for(int i = 0; i < tablero.propiedades.GetLength(0); i++)
            {
                for(int j = 0; j < tablero.propiedades.GetLength(1); j++)
                {
                    if (tablero.propiedades[i,j] != null)
                    {
                        tablero.setearObjeto(casilleroPropiedad, new Vector2(i, j));
                    }
                }
            }

            /*tablero.setearObjeto(casilleroFortuna, new Vector2(7, 0));
            tablero.setearObjeto(casilleroFortuna, new Vector2(8, 10));
            tablero.setearObjeto(casilleroFortuna, new Vector2(0, 4));

            tablero.setearObjeto(casilleroArcaComunal, new Vector2(2, 0));
            tablero.setearObjeto(casilleroArcaComunal, new Vector2(10, 7));
            tablero.setearObjeto(casilleroArcaComunal, new Vector2(0, 7));*/          

            return tablero;
        }

        public void CargarPropiedades(Tablero tablero)
        {

            Barrio bMarron = new Barrio("Marron");
            bMarron.AgregarPropiedad(new Propiedad("Propiedad 1", 60, 2, new int[] { 10, 30, 90, 160 }, 250, 50, 50));
            bMarron.AgregarPropiedad(new Propiedad("Propiedad 2", 80, 4, new int[] { 20, 60, 180, 320 }, 450, 50, 50));
            tablero.agregarBarrio(bMarron);

            Barrio bCeleste = new Barrio("Celeste");
            bCeleste.AgregarPropiedad(new Propiedad("Propiedad 3", 100, 6, new int[] { 30, 90, 270, 400 }, 550, 50, 50));
            bCeleste.AgregarPropiedad(new Propiedad("Propiedad 4", 100, 6, new int[] { 30, 90, 270, 400 }, 550, 50, 50));
            bCeleste.AgregarPropiedad(new Propiedad("Propiedad 5", 120, 8, new int[] { 40, 100, 300, 450 }, 600, 50, 50));
            tablero.agregarBarrio(bCeleste);

            Barrio bMagenta = new Barrio("Magenta");
            bMagenta.AgregarPropiedad(new Propiedad("Propiedad 6", 140, 10, new int[] { 50, 150, 450, 625 }, 750, 100, 100));
            bMagenta.AgregarPropiedad(new Propiedad("Propiedad 7", 140, 10, new int[] { 50, 150, 450, 625 }, 750, 100, 100));
            bMagenta.AgregarPropiedad(new Propiedad("Propiedad 8", 160, 12, new int[] { 60, 180, 500, 700 }, 900, 100, 100));
            tablero.agregarBarrio(bMagenta);

            Barrio bNaranja = new Barrio("Naranja");
            bNaranja.AgregarPropiedad(new Propiedad("Propiedad 9", 180, 14, new int[] { 70, 200, 550, 750 }, 950, 100, 100));
            bNaranja.AgregarPropiedad(new Propiedad("Propiedad 10", 180, 14, new int[] { 70, 200, 550, 750 }, 950, 100, 100));
            bNaranja.AgregarPropiedad(new Propiedad("Propiedad 11", 180, 16, new int[] { 80, 220, 600, 800 }, 1000, 100, 100));
            tablero.agregarBarrio(bNaranja);

            Barrio bRojo = new Barrio("Rojo");
            bRojo.AgregarPropiedad(new Propiedad("Propiedad 12", 220, 18, new int[] { 90, 250, 700, 875 }, 1050, 150, 150));
            bRojo.AgregarPropiedad(new Propiedad("Propiedad 13", 220, 18, new int[] { 90, 250, 700, 875 }, 1050, 150, 150));
            bRojo.AgregarPropiedad(new Propiedad("Propiedad 14", 240, 20, new int[] { 100, 300, 750, 925 }, 1100, 150, 150));
            tablero.agregarBarrio(bRojo);

            Barrio bAmarillo = new Barrio("Amarillo");
            bAmarillo.AgregarPropiedad(new Propiedad("Propiedad 15", 260, 22, new int[] { 110, 330, 800, 975 }, 1150, 150, 150));
            bAmarillo.AgregarPropiedad(new Propiedad("Propiedad 16", 260, 22, new int[] { 110, 330, 800, 975 }, 1150, 150, 150));
            bAmarillo.AgregarPropiedad(new Propiedad("Propiedad 17", 280, 24, new int[] { 120, 360, 850, 1025 }, 1200, 150, 150));
            tablero.agregarBarrio(bAmarillo);

            Barrio bVerde = new Barrio("Verde");
            bVerde.AgregarPropiedad(new Propiedad("Propiedad 18", 300, 26, new int[] { 130, 390, 900, 1100 }, 1275, 200, 200));
            bVerde.AgregarPropiedad(new Propiedad("Propiedad 19", 300, 26, new int[] { 130, 390, 900, 1100 }, 1275, 200, 200));
            bVerde.AgregarPropiedad(new Propiedad("Propiedad 20", 320, 28, new int[] { 150, 450, 1000, 1200 }, 1400, 200, 200));
            tablero.agregarBarrio(bVerde);

            Barrio bAzul = new Barrio("Azul");
            bAzul.AgregarPropiedad(new Propiedad("Propiedad 21", 350, 35, new int[] { 175, 500, 1100, 1300 }, 1500, 200, 200));
            bAzul.AgregarPropiedad(new Propiedad("Propiedad 22", 400, 50, new int[] { 200, 600, 1400, 1700 }, 2000, 200, 200));
            tablero.agregarBarrio(bAzul);


            tablero.setearPropiedad(bMarron.LstPropiedades[0], new Vector2(1, 0));
            tablero.setearPropiedad(bMarron.LstPropiedades[1], new Vector2(3, 0));

            tablero.setearPropiedad(bCeleste.LstPropiedades[0], new Vector2(6, 0));
            tablero.setearPropiedad(bCeleste.LstPropiedades[1], new Vector2(8, 0));
            tablero.setearPropiedad(bCeleste.LstPropiedades[2], new Vector2(9, 0));

            tablero.setearPropiedad(bMagenta.LstPropiedades[0], new Vector2(10, 1));
            tablero.setearPropiedad(bMagenta.LstPropiedades[1], new Vector2(10, 3));
            tablero.setearPropiedad(bMagenta.LstPropiedades[2], new Vector2(10, 4));

            tablero.setearPropiedad(bNaranja.LstPropiedades[0], new Vector2(10, 6));
            tablero.setearPropiedad(bNaranja.LstPropiedades[1], new Vector2(10, 8));
            tablero.setearPropiedad(bNaranja.LstPropiedades[2], new Vector2(10, 9));

            tablero.setearPropiedad(bRojo.LstPropiedades[0], new Vector2(9, 10));
            tablero.setearPropiedad(bRojo.LstPropiedades[1], new Vector2(7, 10));
            tablero.setearPropiedad(bRojo.LstPropiedades[2], new Vector2(6, 10));

            tablero.setearPropiedad(bAmarillo.LstPropiedades[0], new Vector2(4, 10));
            tablero.setearPropiedad(bAmarillo.LstPropiedades[1], new Vector2(3, 10));
            tablero.setearPropiedad(bAmarillo.LstPropiedades[2], new Vector2(1, 10));

            tablero.setearPropiedad(bVerde.LstPropiedades[0], new Vector2(0, 9));
            tablero.setearPropiedad(bVerde.LstPropiedades[1], new Vector2(0, 8));
            tablero.setearPropiedad(bVerde.LstPropiedades[2], new Vector2(0, 6));

            tablero.setearPropiedad(bAzul.LstPropiedades[0], new Vector2(0, 3));
            tablero.setearPropiedad(bAzul.LstPropiedades[1], new Vector2(0, 1));
        }

        public void CargarServicios(Tablero tablero)
        {
            Servicio sAgua = new Servicio("Agua", 150, 8);
            Servicio sElectricidad = new Servicio("Electricidad", 150, 8);

            tablero.setearServicio(sAgua, new Vector2(10, 2));
            tablero.setearServicio(sElectricidad, new Vector2(2, 10));
        }

        public void CargarFerrocarriles(Tablero tablero)
        {
            Ferrocarril fSur = new Ferrocarril("Sur", 200, 50, new int[] { 50, 100, 150, 200 });
            Ferrocarril fOeste = new Ferrocarril("Oeste", 200, 50, new int[] { 50, 100, 150, 200 });
            Ferrocarril fNorte = new Ferrocarril("Norte", 200, 50, new int[] { 50, 100, 150, 200 });
            Ferrocarril fEste = new Ferrocarril("Este", 200, 50, new int[] { 50, 100, 150, 200 });

            tablero.setearFerrocarril(fEste, new Vector2(0, 5));
            tablero.setearFerrocarril(fNorte, new Vector2(5, 10));
            tablero.setearFerrocarril(fOeste, new Vector2(10, 5));
            tablero.setearFerrocarril(fSur, new Vector2(5, 0));
        }


    }
}