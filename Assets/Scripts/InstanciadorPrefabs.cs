﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Classes
{
    public class InstanciadorPrefabs : MonoBehaviour
    {
        public static InstanciadorPrefabs instancia;

        #region Colores de Materiales
        public Material marron;
        public Material celeste;
        public Material magenta;
        public Material naranja;
        public Material rojo;
        public Material amarillo;
        public Material verde;
        public Material azul;
        #endregion

        public List<Material> damelstMaterialesMonopoly()
        {
            List<Material> materiales = new List<Material>();
            materiales.Add(marron);
            materiales.Add(celeste);
            materiales.Add(magenta);
            materiales.Add(naranja);
            materiales.Add(rojo);
            materiales.Add(amarillo);
            materiales.Add(verde);
            materiales.Add(azul);        

            return materiales;
        }

            void Awake()
        {
            if(instancia == null)
            {
                instancia = this;
            }
            else
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
        }

        #region Graficar Tablero Base
        public void GraficarCasilleros(Tablero tablero, GameObject casillero)
        {
            float offset = 0.25f;
            for (int i = 1; i < tablero.casilleros.GetLength(0) - 1; i++)
            {
                Instantiate(casillero, new Vector3(0 - offset, 0.0f, i), Quaternion.Euler(0, 90, 0));
                Instantiate(casillero, new Vector3(tablero.casilleros.GetLength(0) - 1 + offset, 0.0f, i), Quaternion.Euler(0, 270, 0));
            }
            for (int i = 1; i < tablero.casilleros.GetLength(1) - 1; i++)
            {
                Instantiate(casillero, new Vector3(i, 0.0f, 0 - offset), Quaternion.Euler(0, 180, 0));
                Instantiate(casillero, new Vector3(i, 0.0f, tablero.casilleros.GetLength(1) - 1 + offset), Quaternion.Euler(0, 0, 0));
            }
        }

        public void GraficarCasillerosEsquina(Tablero tablero, GameObject casilleroEsquina)
        {
            float offset = 0.25f;
            Instantiate(casilleroEsquina, new Vector3(0 - offset, 0.0f, 0 - offset), Quaternion.Euler(0, 90, 0));
            Instantiate(casilleroEsquina, new Vector3(0 - offset, 0.0f, tablero.casilleros.GetLength(1) - 1 + offset), Quaternion.Euler(0, 0, 0));
            Instantiate(casilleroEsquina, new Vector3(tablero.casilleros.GetLength(0) - 1 + offset, 0.0f, tablero.casilleros.GetLength(1) - 1 + offset), Quaternion.Euler(0, 270, 0));
            Instantiate(casilleroEsquina, new Vector3(tablero.casilleros.GetLength(0) - 1 + offset, 0.0f, 0 - offset), Quaternion.Euler(0, 180, 0));
        }

        #endregion

        #region Graficar Terrenos

        public void GraficarCasillerosPropiedad(Tablero tablero, GameObject casillerosBarrio, GameObject terrenoNombre, GameObject terrenoValor)
        {
            for (int i = 0; i < tablero.propiedades.GetLength(0); i++)
            {
                for (int j = 0; j < tablero.propiedades.GetLength(1); j++)
                {
                    if(tablero.damePropiedad(new Vector2(i, j)) != null
                        )
                    {
                        float offset = 0.275f;
                        GameObject obj = casillerosBarrio;
                        if (tablero.damePropiedad(new Vector2(i, j)).Color != null)
                        {
                            obj.GetComponent<MeshRenderer>().material =
                                damelstMaterialesMonopoly().
                                Find(x => x.name == tablero.damePropiedad(new Vector2(i, j)).Color);

                            terrenoNombre.GetComponent<TextMesh>().text = tablero.damePropiedad(new Vector2(i, j)).Nombre;
                            terrenoNombre.GetComponent<TextMesh>().fontSize = 20;
                            terrenoNombre.GetComponent<Transform>().localScale = new Vector3(0.07f, 0.07f, 1);
                            terrenoValor.GetComponent<TextMesh>().text = "$" + tablero.damePropiedad(new Vector2(i, j)).Valor.ToString();
                            terrenoValor.GetComponent<TextMesh>().fontSize = 20;
                            terrenoValor.GetComponent<Transform>().localScale = new Vector3(0.07f, 0.07f, 1);
                        }
                        if (i == 0)
                        {
                            Instantiate(obj, new Vector3(j, 0.16f, i + offset), Quaternion.Euler(0, 0, 0));
                            Instantiate(terrenoNombre, new Vector3(j, 0.12f, i - 0.1f), Quaternion.Euler(90, 0, 0));
                            Instantiate(terrenoValor, new Vector3(j, 0.12f, i - 0.8f), Quaternion.Euler(90, 0, 0));
                        }
                        if(j == tablero.casilleros.GetLength(1) - 1)
                        {
                            Instantiate(obj, new Vector3(j - offset, 0.16f, i), Quaternion.Euler(0, 90, 0));
                            Instantiate(terrenoNombre, new Vector3(j + 0.1f, 0.12f, i), Quaternion.Euler(90, 90, 0));
                            Instantiate(terrenoValor, new Vector3(j + 0.8f, 0.12f, i), Quaternion.Euler(90, 90, 0));
                        }
                        if(i == tablero.casilleros.GetLength(0) - 1)
                        {
                            Instantiate(obj, new Vector3(j, 0.16f, i - offset), Quaternion.Euler(0, 1800, 0));
                            Instantiate(terrenoNombre, new Vector3(j, 0.12f, i + 0.1f), Quaternion.Euler(90, 180, 0));
                            Instantiate(terrenoValor, new Vector3(j, 0.12f, i + 0.8f), Quaternion.Euler(90, 180, 0));
                        }
                        if (j == 0)
                        {
                            Instantiate(obj, new Vector3(j + offset, 0.16f, i), Quaternion.Euler(0, 90, 0));
                            Instantiate(terrenoNombre, new Vector3(j - 0.1f, 0.12f, i), Quaternion.Euler(90, 90, 0));
                            Instantiate(terrenoValor, new Vector3(j - 0.8f, 0.12f, i), Quaternion.Euler(90, 90, 0));
                        }
                    }
                }
            }
        }

        public void GraficarCasillerosFerrocarril(Tablero tablero, GameObject casillerosFerrocarril, GameObject terrenoNombre, GameObject terrenoValor)
        {
            for (int i = 0; i < tablero.ferrocarriles.GetLength(0); i++)
            {
                for (int j = 0; j < tablero.ferrocarriles.GetLength(1); j++)
                {
                    if (tablero.dameFerrocarril(new Vector2(i, j)) != null)
                    {
                        float offsetY = -0.3f;
                        float offset = -0.1f;
                        GameObject obj = casillerosFerrocarril;

                        terrenoNombre.GetComponent<TextMesh>().text = tablero.dameFerrocarril(new Vector2(i, j)).Nombre;
                        terrenoNombre.GetComponent<TextMesh>().fontSize = 20;
                        terrenoNombre.GetComponent<Transform>().localScale = new Vector3(0.07f, 0.07f, 1);
                        terrenoValor.GetComponent<TextMesh>().text = "$" + tablero.dameFerrocarril(new Vector2(i, j)).Valor.ToString();
                        terrenoValor.GetComponent<TextMesh>().fontSize = 20;
                        terrenoValor.GetComponent<Transform>().localScale = new Vector3(0.07f, 0.07f, 1);


                        if (i == 0)
                        {
                            Instantiate(obj, new Vector3(j - offset, 0.16f, i + offsetY), Quaternion.Euler(0, 0, 0));
                            Instantiate(terrenoNombre, new Vector3(j , 0.12f, i + 0.23f), Quaternion.Euler(90, 0, 0));
                            Instantiate(terrenoValor, new Vector3(j , 0.12f, i - 0.8f), Quaternion.Euler(90, 0, 0));
                        }
                        if (j == tablero.casilleros.GetLength(1) - 1)
                        {
                            Instantiate(obj, new Vector3(j - offsetY, 0.16f, i - offset), Quaternion.Euler(0, 270, 0));
                            Instantiate(terrenoNombre, new Vector3(j - 0.23f, 0.12f, i), Quaternion.Euler(90, 270, 0));
                            Instantiate(terrenoValor, new Vector3(j + 0.8f, 0.12f, i), Quaternion.Euler(90, 270, 0));
                        }
                        if (i == tablero.casilleros.GetLength(0) - 1)
                        {
                            Instantiate(obj, new Vector3(j + offset, 0.16f, i - offsetY), Quaternion.Euler(0, 180, 0));
                            Instantiate(terrenoNombre, new Vector3(j , 0.12f, i - 0.23f), Quaternion.Euler(90, 180, 0));
                            Instantiate(terrenoValor, new Vector3(j , 0.12f, i + 0.8f), Quaternion.Euler(90, 180, 0));
                        }
                        if (j == 0)
                        {
                            Instantiate(obj, new Vector3(j + offsetY, 0.16f, i + offset), Quaternion.Euler(0, 90, 0));
                            Instantiate(terrenoNombre, new Vector3(j + 0.23f, 0.12f, i), Quaternion.Euler(90, 90, 0));
                            Instantiate(terrenoValor, new Vector3(j - 0.8f, 0.12f, i ), Quaternion.Euler(90, 90, 0));
                        }
                    }
                }
            }
        }

        public void GraficarCasillerosServicio(Tablero tablero, GameObject casillerosServicio)
        {
            for (int i = 0; i < tablero.servicios.GetLength(0); i++)
            {
                for (int j = 0; j < tablero.servicios.GetLength(1); j++)
                {
                    if (tablero.dameServicio(new Vector2(i, j)) != null)
                    {
                        GameObject obj = casillerosServicio;
                        Transform terrenoNombre = obj.transform.Find("TerrenoNombre");
                        Transform terrenoValor = obj.transform.Find("TerrenoValor");

                        Transform icono;

                        switch (tablero.dameServicio(new Vector2(i, j)).Nombre)
                        {
                            case "Agua":
                                icono = obj.transform.Find("Grifo");
                                break;
                            case "Electricidad":
                                icono = obj.transform.Find("Rayo");
                                break;
                            default:
                                icono = obj.transform.Find("Grifo");
                                break;
                        }

                        icono.gameObject.SetActive(true);

                        terrenoNombre.GetComponent<TextMesh>().text = tablero.dameServicio(new Vector2(i, j)).Nombre;
                        terrenoNombre.GetComponent<TextMesh>().fontSize = 20;
                        terrenoNombre.GetComponent<Transform>().localScale = new Vector3(0.07f, 0.07f, 1);
                        terrenoValor.GetComponent<TextMesh>().text = "$" + tablero.dameServicio(new Vector2(i, j)).Valor.ToString();
                        terrenoValor.GetComponent<TextMesh>().fontSize = 20;
                        terrenoValor.GetComponent<Transform>().localScale = new Vector3(0.07f, 0.07f, 1);

                        int angular = 0;

                        if (i == 0)
                        {
                            angular = 180;
                        }
                        if (j == tablero.casilleros.GetLength(1) - 1)
                        {
                            angular = 90;
                        }
                        if (i == tablero.casilleros.GetLength(0) - 1)
                        {
                            angular = 0;
                        }
                        if (j == 0)
                        {
                            angular = 270;
                        }

                        Instantiate(obj, new Vector3(j, 0, i), Quaternion.Euler(0, angular, 0));
                        icono.gameObject.SetActive(false);
                    }
                }
            }
        }
        #endregion

        #region Graficar Fichas: Posicionamiento y Calculo de Offsets
        public void GraficarFichasJugadores(Tablero tablero, List<Jugador> jugadores, GameObject fichaJugador)
        {
            int index = 0;

            foreach (Jugador jugador in jugadores)
            {
                Vector2 offset = dameOffset(tablero, jugador, index);

                fichaJugador.GetComponent<MeshRenderer>().material =
                               damelstMaterialesMonopoly().
                               Find(x => x.name == jugador.Color);

                fichaJugador.tag = fichaJugador.GetComponent<MeshRenderer>().sharedMaterial.name;

                Instantiate(fichaJugador, new Vector3(jugador.Posicion.x + offset.x, 0.4f, jugador.Posicion.y + offset.y), Quaternion.identity);

                index++;
            }
        }

        public void TrasladarFicha(Tablero tablero, List<Jugador> jugadores, Jugador jugador)
        {
            GameObject fichaJugador = GameObject.FindGameObjectWithTag(jugador.Color);

            int matches = 0;
            foreach (Jugador tmpJugador in jugadores)
            {
                if (jugador.Nombre != tmpJugador.Nombre && jugador.Posicion == tmpJugador.Posicion)
                {
                    ReubicarFicha(tablero, jugadores, tmpJugador, matches);
                    matches++;
                }
            }

            Vector2 offset = dameOffset(tablero, jugador, matches);

            fichaJugador.transform.position = new Vector3(jugador.Posicion.y + offset.x, 0.4f, jugador.Posicion.x + offset.y);
        }

        public void ReubicarFicha(Tablero tablero, List<Jugador> jugadores, Jugador jugador, int matches)
        {
            GameObject fichaJugador = GameObject.FindGameObjectWithTag(jugador.Color);

            Vector2 offset = dameOffset(tablero, jugador, matches);

            fichaJugador.transform.position = new Vector3(jugador.Posicion.y + offset.x, 0.4f, jugador.Posicion.x + offset.y);
        }

        public Vector2 dameOffset(Tablero tablero, Jugador jugador, int matches)
        {
            Vector2 offset = new Vector2();

            float extSur = 0;
            float extOeste = tablero.casilleros.GetLength(0) - 1;
            float extNorte = tablero.casilleros.GetLength(1) - 1;
            float extEste = 0;

            int esteOeste = 0; //-1 para Este // +1 para Oeste //
            int norteSur = 0;  //-1 para Norte// +1 para Sur   //

            int contadorLaterales = 0;

            if (jugador.Posicion.y == extSur)
            {
                norteSur = 1;
                contadorLaterales++;
            }

            if (jugador.Posicion.x == extOeste)
            {
                esteOeste = 1;
                contadorLaterales++;
            }
            
            if (jugador.Posicion.y == extNorte)
            {
                norteSur = -1;
                contadorLaterales++;
            }
            
            if (jugador.Posicion.x == extEste)
            {
                esteOeste = -1;
                contadorLaterales++;
            }

            if (contadorLaterales == 2)
            {
                offset = new Vector2(norteSur * (0.05f - (0.3f * (matches / 2))), esteOeste * 0.45f);

                if (matches != 0 && matches % 2 == 0)
                {
                    offset = new Vector2(offset.x, esteOeste * 0.45f);
                }
                else if (matches != 0 && matches % 2 == 1)
                {
                    offset = new Vector2(offset.x, esteOeste * 0.05f);
                }
                return offset;
            }
            else if (contadorLaterales == 1)
            {
                if (esteOeste == 0)
                {
                    offset = new Vector2(norteSur * (-0.15f - (0.3f * (matches / 2))), -0.2f);

                    if (matches != 0 && matches % 2 == 0)
                    {
                        offset = new Vector2(offset.x, -0.2f);
                    }
                    else if (matches != 0 && matches % 2 == 1)
                    {
                        offset = new Vector2(offset.x, 0.2f);
                    }
                    return offset;
                }
                else if (norteSur == 0)
                {
                    offset = new Vector2(-0.2f, -esteOeste * (-0.15f - (0.3f * (matches / 2))));

                    if (matches != 0 && matches % 2 == 0)
                    {
                        offset = new Vector2(-0.2f, offset.y);
                    }
                    else if (matches != 0 && matches % 2 == 1)
                    {
                        offset = new Vector2(0.2f, offset.y);
                    }
                    return offset;
                }
                else
                {
                    return new Vector2(0, 0);
                }
            }
            else
            {
                return new Vector2(0, 0);
            }
        }

        public void EliminarFicha(Jugador jugador)
        {
            GameObject.Destroy(GameObject.FindGameObjectWithTag(jugador.Color));
        }
        #endregion
    }
}